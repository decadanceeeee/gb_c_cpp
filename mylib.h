//
// Created by aleksandr on 07.04.2021.
//

#ifndef GB_C_CPP_MYLIB_H
#define GB_C_CPP_MYLIB_H

void init_array(float arr[], size_t size);
void print_array(float arr[], size_t size);
void elements_count(float arr[], size_t size, size_t* less_than_zero, size_t* more_than_zero);

#endif //GB_C_CPP_MYLIB_H

//
// Created by aleksandr on 07.04.2021.
//

/*
Создайте проект из 2х cpp файлов и заголовочного (main.cpp, mylib.cpp, mylib.h) во втором модуле mylib объявить 3 функции: для инициализации массива (типа float), печати его на экран и подсчета количества отрицательных и положительных элементов. Вызывайте эти 3-и функции из main для работы с массивом.
Описать макрокоманду (через директиву define), проверяющую, входит ли переданное ей число (введенное с клавиатуры) в диапазон от нуля (включительно) до переданного ей второго аргумента (исключительно) и возвращает true или false, вывести на экран «true» или «false».
Задайте массив типа int. Пусть его размер задается через директиву препроцессора #define. Инициализируйте его через ввод с клавиатуры. Напишите для него свою функцию сортировки (например Пузырьком). Реализуйте перестановку элементов как макрокоманду SwapINT(a, b). Вызывайте ее из цикла сортировки.
*/

#include <iostream>

#include "mylib.h"

#define IN(value, a) ((value) < (a)) && ((value) > 0) ? true : false
#define SWAP(a, b) { a ^= b; \
                     b ^= a; \
                     a ^= b; }

#define ARRAY_SIZE 10

void bubblesort(int *arr, size_t size)
{
    for (int i = 0; i < size; ++i) {
        for (int j = 1; j < size; ++j)
            if (arr[j - 1] > arr[j]) {
                SWAP(arr[j - 1], arr[j]);
            }
    }
}

int main() {
    int a = 5;
    int b = 6;
    SWAP(a, b);
    int* arr = new int[ARRAY_SIZE];
    for (size_t i = 0; i < ARRAY_SIZE; ++i) {
        std::cin >> arr[i];
    }
    bubblesort(arr, ARRAY_SIZE);

    for (size_t i = 0; i < ARRAY_SIZE; ++i) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;


    delete [] arr;
}
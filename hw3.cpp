//
// Created by aleksandr on 24.03.2021.
//

#include <iostream>

float task_1(int a, int b, int c, int d) {
    return a * (b + static_cast<float>(c) / d);
}

int task_2(int a) {
    return ((a <= 21) ? 21 - a : 2 * (a - 21));
}

int main() {
    std::cout << task_1(10, 5, 8, 3) << std::endl;
    std::cout << task_2(26) << std::endl;
    std::cout << task_2(-25746) << std::endl;
    return 0;
}

//
// Created by aleksandr on 01.04.2021.
//

/*
Выделить в памяти динамический одномерный массив типа int. Размер массива запросить у пользователя. Инициализировать его числами степенями двойки: 1, 2, 4, 8, 16, 32, 64, 128 … Вывести массив на экран. Не забыть освободить память. =) Разбить программу на функции.
Динамически выделить матрицу 4х4 типа int. Инициализировать ее псевдослучайными числами через функцию rand. Вывести на экран. Разбейте вашу программу на функции которые вызываются из main.
Написать программу c 2-я функциями, которая создаст два текстовых файла (*.txt), примерно по 50-100 символов в каждом (особого значения не имеет с каким именно содержимым). Имена файлов запросить у польлзователя.
*/

#include <iostream>
#include <cstdlib>
#include <fstream> // Для использования ofstream

void task_1() {
    size_t n;
    std::cout << "Введите размер массива...\t";
    std::cin >> n;
    int* arr = new int[n];
    if (arr) {
        *arr = 1;
    }
    for (size_t i = 0; i < n - 1; ++i) {
        arr[i+1] = (arr[i] << 1);
    }
    std::cout << "Итоговый массив: " << std::endl;
    for (size_t i = 0; i < n; ++i) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
    delete [] arr;
}

void task_2() {
    srand(time(nullptr));
    size_t n = 4;
    size_t m = 4;
    int** arr = new int*[n];
    for (size_t i = 0; i < n; ++i) {
        arr[i] = new int[m];
    }
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
            arr[i][j] = rand() % 100;
        }
    }
    std::cout << "Итоговый массив: " << std::endl;
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    for (size_t i = 0; i < n; ++i) {
        delete [] arr[i];
    }
    delete [] arr;
}

void task_3() {
    srand(time(nullptr));
    size_t n = rand() % 50 + 50;
    int* arr = new int[n];
    std::ofstream fout("data.txt");

    for (size_t i = 0; i < n; ++i)
    {
        char c = rand() % 128;
        fout << c << " ";
    }
    fout.close();
    delete [] arr;
}

int main() {
    task_1();
    task_2();
    task_3();
    return 0;
}
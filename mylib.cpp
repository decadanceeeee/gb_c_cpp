//
// Created by aleksandr on 07.04.2021.
//

#include <cstdio>
#include <iostream>

void init_array(float arr[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        std::cin >> arr[i];
    }
}

void print_array(float arr[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

void elements_count(float arr[], size_t size, size_t* less_than_zero, size_t* more_than_zero) {
    *less_than_zero = 0;
    *more_than_zero = 0;
    for (size_t i = 0; i < size; ++i) {
        if (arr[i] < 0) {
            *less_than_zero++;
        }
        if (arr[i] > 0) {
            *more_than_zero++;
        }
    }
}
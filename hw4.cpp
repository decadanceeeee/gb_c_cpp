//
// Created by aleksandr on 24.03.2021.
//

#include <iostream>

bool task_1(int a, int b) {
    return (a + b > 10) && (a + b < 20);
}

bool task_2(int a, int b) {
    return (a == 10 && b == 10) || (a + b == 10);
}

// task_3 in main

bool task_4(int n) {
    if (n <= 1)
        return false;
    for (int i = 2; i * i <= n; ++i)
        if (n % i == 0)
            return false;
    return true;
}

bool task_5(int year) {
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
}


int main() {
    std::cout << (task_1(10, 5) ? "true" : "false") << std::endl;
    std::cout << (task_1(50, 252) ? "true" : "false") << std::endl;

    std::cout << (task_2(10, 10) ? "true" : "false") << std::endl;
    std::cout << (task_2(13, -3) ? "true" : "false") << std::endl;
    std::cout << (task_2(17227, -421) ? "true" : "false") << std::endl;


    // task_3
    for (int i = 1; i <= 50; i+=2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    std::cout << (task_4(127) ? "true" : "false") << std::endl;
    std::cout << (task_4(158618) ? "true" : "false") << std::endl;


    std::cout << (task_5(1900) ? "true" : "false") << std::endl;
    std::cout << (task_5(2020) ? "true" : "false") << std::endl;

    return 0;
}
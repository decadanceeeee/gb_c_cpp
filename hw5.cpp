//
// Created by aleksandr on 01.04.2021.
//

/*
1) Написать функцию которая выводит массив double чисел на экран. Параметры функции это сам массив и его размер. Вызвать эту функцию из main.
2) Задать целочисленный массив, состоящий из элементов 0 и 1. Например: [ 1, 1, 0, 0, 1, 0, 1, 1, 0, 0 ]. Написать функцию, заменяющую в принятом массиве 0 на 1, 1 на 0. Выводить на экран массив до изменений и после.
3) Задать пустой целочисленный массив размером 8. Написать функцию, которая с помощью цикла заполнит его значениями 1 4 7 10 13 16 19 22. Вывести массив на экран.
 */

void task_1(double arr[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        std::cout << arr[i] << " "
    }
    std::cout << std::endl;
}

void task_2(double arr[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        if (arr[i] == 0) {
            arr[i] = 1;
        } else {
            arr[i] = 0;
        }
    }
}

void task_3(int arr[], size_t size) {
    int a = 1;
    for (size_t i = 0; i < size; ++i) {
        arr[i] = a;
        ++a;
    }
}

void main() {
    int arr = [ 1, 1, 0, 0, 1, 0, 1, 1, 0, 0 ];
    task_1(arr, 10);
    task_2(arr, 10);
    task_1(arr, 10);
    int a[8];
    task_3(a, 8);
    task1(a, 8);
}